# -*- coding: utf-8 -*-

from kivy.app import App
from kivy.lang import Builder

main_widget_kv_1 = '''
Button:
    text: 'Hello from formGeneratorTest2'
    Button:
        text: 'Button 2'
    Label:
        text: "Name: "
'''

main_widget_kv_2 = '''
GridLayout:
    cols:1
    size: root.width, root.height

    GridLayout:
        cols:2

        Label:
            text: "Name: "

        TextInput:
            multinline:False

        Label:
            text: "Email: "

        TextInput:
            multiline:False
'''

main_widget_kv_3 = '''
<MyGrid>:
    GridLayout:
        cols:1
        size: root.width, root.height

        GridLayout:
            cols:2

            Label:
                text: "Name: "

            TextInput:
                multinline:False

            Label:
                text: "Email: "

            TextInput:
                multiline:False
'''


class FormGeneratorApp(App):

    def build(self):
        self.title = 'Test generatora formularzy'  # tytuł okna aplikacji
        main_widget = Builder.load_string(main_widget_kv_1)
        main_widget = Builder.load_string(main_widget_kv_2)
        return main_widget


if __name__ == "__main__":
    app = FormGeneratorApp()
    app.run()
