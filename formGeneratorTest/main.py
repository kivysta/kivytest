# based on: https://www.codementor.io/kiok46/beginner-kivy-tutorial-basic-crash-course-for-apps-in-kivy-y2ubiq0gz
# -*- coding: utf-8 -*-
import kivy

kivy.require('1.11.1')

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout

from os import listdir

kv_path = './kv/'
for kv in listdir(kv_path):
    Builder.load_file(kv_path + kv)  # load each .kv file individually


class AddButton(Button):
    pass


class SubstractButton(Button):
    pass


class Container(GridLayout):
    display = ObjectProperty()

    def add_one(self):
        value = int(self.display.text)
        self.display.text = str(value + 1)

    def substract_one(self):
        value = int(self.display.text)
        self.display.text = str(value - 1)


class FormGeneratorApp(App):

    def build(self):
        self.title = 'Test generatora formularzy'  # tytuł okna aplikacji
        return Container()


if __name__ == "__main__":
    app = FormGeneratorApp()
    app.run()
