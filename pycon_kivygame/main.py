# http://www.pagema.net/kivykivent-pl.html
# zamiast cymunk użytoo pymunk
# -*- coding: utf-8 -*-
import kivy
kivy.require('1.11.1')

import defs

from pymunk import Body, Circle, Space, Segment, Vec2d
from random import randint

from kivy.app import App
from kivy.base import EventLoop
from kivy.clock import Clock
from kivy.core.window import Keyboard, Window
from kivy.logger import Logger
from kivy.uix.screenmanager import ScreenManager
from kivy.uix.widget import Widget
from kivy.vector import Vector


class GameScreenManager(ScreenManager):
    pass


class Baloon(Widget):
    pass

class KivyGame(Widget):
    def __init__(self, *a, **kwa):
        super().__init__(*a, **kwa)

        self.baloons = []
        self.widgets_with_bodies = []
        self.walls = []
        self.space = None

        self.app = App.get_running_app()
        Clock.schedule_once(self.init_widgets)

    def init_widgets(self, dt):  # a to się wykona "tuż po"
        for _ in range(defs.num_baloons):
            baloon = Baloon(center=(randint(200, 800), randint(100, 500)))
            self.baloons.append(baloon)
            self.add_widget(baloon)

        Clock.schedule_interval(self.update, 1.0 / 30)
        EventLoop.window.bind(on_key_up=self.on_key_up)

        self.init_physics()


    def init_physics(self):
        self.space = Space()  # cała fizyka dieje się w ramach Space()

        self.init_body(self.fred, 72, defs.fred_collision_type)
        for b in self.baloons:
            self.init_body(b, 50)

        self.create_walls()

        self.space.add_collision_handler(defs.goal_collision_type,
                                         defs.fred_collision_type)  # ,
        # self.goal_reached)

    def create_walls(self):
        segments, R = self.wall_segments()
        for v1, v2 in segments:
            wall = Segment(self.space.static_body, v1, v2, R)
            wall.elasticity = defs.elasticity
            wall.friction = defs.wall_friction
            wall.collision_type = defs.default_collision_type

            self.space.add(wall)
            self.walls.append(wall)

    def init_body(self, widget, r, collision_type=defs.default_collision_type):
        """ initialize cymunk body for given widget as circle
            of radius=r
        """
        widget.body = Body(defs.mass, defs.moment)
        widget.body.position = widget.center
        self.widgets_with_bodies.append(widget)

        shape = Circle(widget.body, r)
        shape.elasticity = defs.elasticity
        shape.friction = defs.friction
        shape.collision_type = defs.default_collision_type

        self.space.add(widget.body)
        self.space.add(shape)

    def update(self, dt):
        for b in self.baloons:
            b.body.apply_impulse_at_local_point((randint(-10, 10), randint(-10, 10)))

        self.space.step(1.0 / 30)

        for w in self.widgets_with_bodies:
            w.center = tuple(w.body.position)

    def on_resize(self, _win, w, h):
        if not self.walls:
            return
        Logger.debug("move walls with witgh w=%s h=%s", w, h)

        segments, __R = self.wall_segments()

        for (v1, v2), wall in zip(segments,
                                  self.walls):
            wall.a = v1
            wall.b = v2

        self.space.reindex_static()


    def on_key_up(self, __window, key, *__, **___):
        # code = Keyobard.keycode_to_string)None, key) # można tak, ale to brzydki hack
        dx, dy = 0, 0
        if key == Keyboard.keycodes['up']:
            dy = +5
        if key == Keyboard.keycodes['down']:
            dy = -5
        if key == Keyboard.keycodes['left']:
            dx = -5
        if key == Keyboard.keycodes['right']:
            dx = +5

        self.fred.body.apply_impulse(Vector(dx, dy))

    def on_touch_up(self, touch):
        vdir = Vector(touch.pos) - self.fred.center  # wynik to Vector
        #self.fred.body.apply_impulse_at_local_point(vdir * 5)
        self.fred.body.apply_impulse_at_world_point(vdir * 5)

    # def goal_reached(self, _space, _arbiter):
    def goal_reached(self):
        self.app.sm.current = 'success'


    def wall_segments(self):
        w, h = self.size
        R = 200
        return [(Vec2d(-2 * R, -R), Vec2d(w + 2 * R, -R)),
                (Vec2d(-R, -2 * R), Vec2d(-R, h + 2 * R)),
                (Vec2d(-2 * R, h + R), Vec2d(w + 2 * R, h + R)),
                (Vec2d(w + R, h + 2 * R), Vec2d(w + R, -2 * R))], R


class KivyGameApp(App):
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.sm = None

    def build(self):
        self.sm = GameScreenManager()
        return self.sm



if __name__ == '__main__':
    KivyGameApp().run()